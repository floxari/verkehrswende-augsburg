---
layout: page
title: Nulltarif
permalink: /nulltarif/
nav_order: 80
---


## Kostenloser ÖPNV - Nulltarif

Hohe Umstiegsquoten lassen sich auch durch den **Nulltarif**, also einen ÖPNV, für dessen Nutzung keinen Tickets notwendig sind, erzielen. 

Eine **gerechte Mobilität für Alle** braucht zusätzlich zum flächendeckenden Netz aus Fahrradstraße, Bahn- und Buslinien den Nulltarif. Städte ohne Fahrscheine gibt es weltweit, z.B. in Dünkirchen, Aubagne, Nieort und über zwanzig weiteren Orten Frankreichs, in der estnischen Hauptstadt Tallinn (inzwischen auf fast ganz Estland ausgeweitet), in mehreren polnischen Städten (bei Smog sogar in Warschau und Krakau) und seit 2020 in ganz Luxemburg. Deutschland hinkt mit einer Kleinstadt (Pfaffenhofen) hinterher. Templin brach den Versuch ab – wegen zu großen Erfolgs! In Tübingen fährt der Bus samstags ticketfrei, in manch anderen Städten an Adventssamstagen. In Augsburg gibt es seit 2020 in der CityZone, also eine Station rund um Königsplatz und Moritzplatz den kostenlosen Nahverkehr. Das war’s.

**Eine Ökopolitik der Reichen lehnen wir ab.** Mobilität ist wichtig für ein gutes Leben, die Teilhabe am gesellschaftlichen Leben und sollte für alle zugänglich sein. Fahrverbote oder E-Autos sind keine Lösung. Wir brauchen einen lebenswerte Umwelt und eine bunte Gesellschaft – keine Verdrängung, Ausgrenzung  oder ein gutes Gewissen für die, die es sich leisten können.
Mit der freien Möglichkeit mobil zu sein, wird auch für junge Menschen und Menschen ohne Führerschein das Leben außerhalb der Ballungszentren attraktiver.


## Und wer soll das bezahlen?
Dass wir noch keinen Nulltarif haben, ist keine Frage des mangelnden Geldes, sondern des politischen Willens. Laut dem Verband deutscher Verkehrsunternehmen wären jährlich 12 Milliarden Euro notwendig, würde der Staat die Fahrpreise vollständig erstatten. Zur Kostendeckung können Gelder der Pendler*innenpauschale, Diesel- , Dienstwagen- und Dienstwagenprivatnutzungs-Subventionierung umgewidmet werden oder würden sogar überflüssig, welche den Staat jährlich ca. 20,6 Milliarden Euro kosten. Zusätzlich würden Kosten des Fahrscheinwesens, der Strafverfolgung, der Autoinfrastruktur und im Gesundheitswesen wegfallen.
Nulltarif finanziert sich selbst!
Weitere Finanzierungsmodelle existieren auch auf kommunaler Ebene, z.B. eine Nahverkehrsabgabe für Gewerbetreibende, welche vom Nulltarif auch maßgeblich profitieren würden, einer City-Maut, oder erhöhten Parkplatzgebühren. Überschüsse können in den Ausbau des ÖPNV, der Fuß- und Radwege Infrastruktur und einen entgeldlosen Fernverkehr gesteckt werden

![Grafik: NulltarifFinanzierung](/assets/verkehr_bilder_finanzierung.jpg)

## Nulltarif in und um Augsburg

- **Bericht über die kostenfreie City Zone**
[https://www.augsburger-allgemeine.de/augsburg/Augsburg-City-Zone-So-funktioniert-der-kostenlose-Innenstadt-Nahverkehr-in-Augsburg-id55408171.html](https://www.augsburger-allgemeine.de/augsburg/Augsburg-City-Zone-So-funktioniert-der-kostenlose-Innenstadt-Nahverkehr-in-Augsburg-id55408171.html)

- Bericht über zu wenig Fahrgäste 2022, als Folge soll der Takt reduziert werden: Leser\*innen begründen die fehlende Attraktivität des ÖPNV hier primär mit der unattraktiven Preisgestaltung - eine Änderung ist also notwendig:
[https://www.augsburger-allgemeine.de/augsburg/augsburg-oedp-bekraeftigt-forderung-nach-rueckkehr-zum-fuenf-minuten-takt-id61841931.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-oedp-bekraeftigt-forderung-nach-rueckkehr-zum-fuenf-minuten-takt-id61841931.html)
[https://www.augsburger-allgemeine.de/augsburg/augsburg-der-stau-ist-zurueck-in-augsburg-so-hat-sich-die-mobilitaet-2021-entwickelt-id61696731.html](https://www.augsburger-allgemeine.de/augsburg/augsburg-der-stau-ist-zurueck-in-augsburg-so-hat-sich-die-mobilitaet-2021-entwickelt-id61696731.html)

- Franz Bossek, Direktkandidat von Bündnis 90/DIE GRÜNEN im Wahlkreis Augsburg Land zur Bundestagswahl 2017, forderte den Nulltarif: [https://www.augsburger-allgemeine.de/augsburg-land/Bundestag-Er-will-Bus-und-Tram-kostenlos-machen-id42638241.html](https://www.augsburger-allgemeine.de/augsburg-land/Bundestag-Er-will-Bus-und-Tram-kostenlos-machen-id42638241.html)


## Umsetzung des Nulltarif

### Sofort:

Schnelle Aufwertung des ÖPNV durch:
- Nulltarif am Wochenende und an Feiertagen,
- **Ganztägiges 365€-Ticket für alle** für den gesamten AVV-Verbundraum ([https://www.avv-augsburg.de/fileadmin/user_upload/Netzplaene_und_Karten/avv_tarifzonenplan_verbundraum_2021_12_web.pdf](https://www.avv-augsburg.de/fileadmin/user_upload/Netzplaene_und_Karten/avv_tarifzonenplan_verbundraum_2021_12_web.pdf)) nach Vorbild des bereits jetzt erhältlichen 365€ Tickets für Schüler und Auszubildende ([https://www.avv-augsburg.de/fahrtauskunft/tickets-tarife/365eur-ticket](https://www.avv-augsburg.de/fahrtauskunft/tickets-tarife/365eur-ticket)).
- **Ausweitung des Nutzungsraumes des Semestertickets** für alle an der Universität Augsburg oder an der Hochschule Augsburg immatrikulierten Studierenden auf den gesamten AVV-Verbundraum ohne Preiserhöhung.
- **Freifahrschein für den gesamten AVV-Verbundraum für alle Senior*\innen, die ihren Führerschein abgeben.** Dies ist aktuell abgeschwächt, erst ab 9 Uhr morgens gültig und nur im Innenraum Zone 10 und 20, schon seit 2019 und nur für Menschen ab 65 Jahren und wohnhaft in der Stadt Augsburg möglich: [https://www.sw-augsburg.de/magazin/detail/fuehrerschein-gegen-jahresabo/](https://www.sw-augsburg.de/magazin/detail/fuehrerschein-gegen-jahresabo/))
- Verzicht auf Strafanzeige wegen “Schwarzfahren” seitens der Stadtwerke Augsburg und des Augsburger Verkehrsverbunds (Ob Strafanzeige erstattet wird, entscheiden die Verkehrsbetriebe selbst)
- Rücknahme aller bereits gestellten Strafanträge
- Vorbereitung der Einführung eines Nulltarifs im gesamten AVV-Gebiet


### 1-2 Jahre später sollte gelingen:
- Nulltarif in Bussen und Bahnen für Fahrgäste mit geringen Einkünften und als Prämie für den Verzicht auf ein privates Auto.
- Menschen unter 18 Jahre, sowie Schüler\*innen, Auszubildende, Arbeitslose, Rentner*innen) fahren im kompletten AVV-Gebiet umsonst

### In den Jahren danach wird alles fertig:
**Der fahrscheinlose ÖPNV (Tram, Bus, S-Bahn und Regionalbahn) wird im gesamten AVV-Gebiet eingeführt.**
Augsburg wird Reallabor und Modell-Großstadt für einen Nulltarif.
    

## Finanzierung: Einsparungen höher als die Kosten
Zunächst zur Größenordnung: "Erstattet der Staat die Fahrpreise vollständig, wären laut Branchenverband VDV zwölf Milliarden Euro jährlich nötig." So zitiert die Berliner Zeitung am 14.2.2018 (Quelle nicht mehr online) die Verkehrsunternehmen. Die werden - aus Profitgründen - eher übertreiben. Gehen wir also mal von dieser Summe aus ... 
Denn: Das ist gar nicht so viel. Allein die direkten Einsparungen erreichen diese Größenordnung. Diese sind: 

- Umwidmung der Pendler\*innenpauschale: Rund 5,2 Milliarden Euro kostet den Staat das Fahren mit Auto oder ÖPNV zur Arbeit schon jetzt - er muss die Kosten ersetzen im Rahmen der Steuererklärungen ([https://www.wiwo.de/politik/deutschland/berufspendler-pendler-kosten-den-fiskus-fuenf-milliarden-euro/20654060.html](https://www.wiwo.de/politik/deutschland/berufspendler-pendler-kosten-den-fiskus-fuenf-milliarden-euro/20654060.html)). Wird der Nulltarif eingeführt, kann die Pendlerpauschale komplett abgeschafft werden. Es wäre absurd, würde der Staat Busse und Bahnen umsonst fahren lassen und dennoch das Autofahren finanzieren. Die behaupteten Fahrkarteneinnahmen der Verkehrsbetriebe sind zu guten Teilen über die Pendlerpauschale finanziert. Das kann der Stadt auch direkt bezahlen, statt über den umständlichen Umweg der Steuererklärung. So oder so: Fast die Hälfte der Nulltarif-Kosten wären allein hiermit abgedeckt.

- Dann fahren unzählige Dienstwagen herum, deren Kosten ebenfalls seitens der Firmen von der Steuer abgesetzt werden können. 4,6 Milliarden sollen das sein. Zumindest ein Teil davon könnte eingespart werden, wenn der Nulltarif kommt ([https://www.klima-allianz.de/fileadmin/user_upload/Dateien/Daten/Publikationen/Positionen/2011_06_Positionspapier_Firmenwagen.pdf](https://www.klima-allianz.de/fileadmin/user_upload/Dateien/Daten/Publikationen/Positionen/2011_06_Positionspapier_Firmenwagen.pdf)).

- Außerdem kommt noch die Subventionierung der Privatnutzung von Dienstwagen hinzu, die laut der Broschüre "Nulltarif für Bus und Bahn" 3 Milliarden Euro pro Jahr beträgt. ([https://www.nachhaltig-links.de/images/DateienJ2/1_Mobilitaet/2_PDF/2018/7307_LinkeBTF_Nulltarif_A5_4c_Web.pdf](https://www.nachhaltig-links.de/images/DateienJ2/1_Mobilitaet/2_PDF/2018/7307_LinkeBTF_Nulltarif_A5_4c_Web.pdf))

- Noch höher sind die Subventionen für Diesel, einst als angeblich umweltfreundliche Art des Autofahrens beim Staat beliebt. 7,8 Milliarden werden da reingesteckt, die bei Einführung des Nulltarifs auch keinen Sinn mehr ergeben. Das sind 2/5 der benötigten Summe! (Quelle nicht mehr online). In der Broschüre "Nulltarif für Bus und Bahn" wird der Betrag mit 8 Milliarden Euro pro Jahr angegeben.

**Weitere Argumente für den fahrscheinlosen ÖPNV: [https://www.nulltarif.siehe.website](https://www.nulltarif.siehe.website)**

(Viele unserer Ideen und Formulierungen sind auch angelehnt oder übernommen von der [Verkehrswende Initiative in Koblenz](https://verkehrswende.koblenz.mobi/) oder [Wolfsburg](https://verkehrswendestadt.de/)).