---
layout: page
title: Mitmachen
permalink: /mitmachen/
nav_order: 100
---

### Du willst mitmachen oder selbst aktiv werden?
Mit Aktionen, politischer Einmischung und auch euren Ideen und Vorschlägen wollen wir die Verkehrswende in und um Augsburg voranbringen. Startet eigene Aktionen in euren Straßen und Wohnvierteln. Wir helfen bei der Gründung weiterer Initiativen in Stadtteilen oder den Orten rund um Augsburg. Gerne kommen wir auch zu euch für Vorträge oder einen Austausch über die Vorschläge.
**Dann schreib eine Mail an [verkehrswende-augsburg@riseup.net](mailto:verkehrswende-augsburg@riseup.net).**

**Komm zu den nächsten Aktionen und spreche uns an!**

### Anmeldung zum Mailverteiler
Einfach eine leere Mail an verkehrswende-augsburg-subscribe@lists.riseup.net senden, dann werdet ihr aufgenommen.

Wenn ihr euch wieder abmelden möchtet, einfach eine leere Mail an verkehrswende-augsburg-unsubscribe@lists.riseup.net senden, dann werdet ihr vom Mailverteiler entfernt.

Wenn ihr selbst Termine über den Mailverteiler teilen wollt einfach an verkehrswende-augsburg@riseup.net schreiben

## Mach mit bei Aktionen
aktuelle Termine gibt es im Reiter "Termine"

**Jeden letzten Freitag im Monat um 18 Uhr am Rathausplatz: Critical Mass**
[https://criticalmass-augsburg.de/](https://criticalmass-augsburg.de/)
Eine Gruppe von Radfahrer\*innen fährt gemeinsam durch die Stadt – Wir sind der Verkehr!

![Grafik: Critical Mass Plakat](/assets/CM-Western.jpg)

**Radeln zur Uni - Wir fahren gemeinsam als Critical Mass jeden Tag zur Uni**
++Aktion ist aktuell leider nicht aktiv, wartet auf motivierte Menschen zur Reaktivierung++
Treffpunkte, Uhrzeit und Orte finden sich auf der Homepage:
[https://www.radeln-zur-uni.de/](https://www.radeln-zur-uni.de/)

**Im Klimacamp am Fischmarkt** kannst du bei jedem Wetter, zu jeder Jahrezeit und den ganzen Tag über zeigen, dass du mit der Klima- und Verkehrspolitik in Augsburg und Deutschlandweit unzufrieden bist und deswegen vor dem Rathaus für Klimagerechtigkeit campen, denn wir campen bis ihr handelt, wir schlafen weil die Politiker\*innen die Klimakrise verschlafen.  [https://augsburg.klimacamp.eu/](https://augsburg.klimacamp.eu/)

Außerdem gibt es auch ein **Buntes Workshop- und Vortragsprogramm im Klimacamp am Fischmarkt**
[https://augsburg.klimacamp.eu/programm/](https://augsburg.klimacamp.eu/programm/)

## Projekte für selbstorganisierte Fahrradressourcen in Augsburg:

**Fahrradselbsthilfewerkstatt Bikekitchen**
Heilig-Kreuz-Straße 30 (Do. 18-20 Uhr) 
[http://www.bikekitchen-augsburg.de/](http://www.bikekitchen-augsburg.de/)

**Lastenradverleih Max & Moritz – die Statt Transporter zum Ausleihen**
[https://max-und-moritz.bike/](https://max-und-moritz.bike/)




