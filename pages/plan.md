---
layout: page
title: Verkehrswendeplan
permalink: /plan/
nav_order: 20
---

## Der Verkehrswendeplan
Gemeinsam erstellen wir einen Verkehrswendeplan für Augsburg, der umgesetzt werden soll und den Autoverkehr deutlich verringert sowie Fuß-, Fahrrad- und ÖPNV-Verbindungen erheblich ausbaut. Auf dieser Seite findest du die aktuelle Version des “Verkehrswendeplans von Unten”. Bring auch du dich mit deinen Ideen und Vorschlägen ein!

## Karte
[![Plan Vorschaubild](/assets/plan_v1_k.png)](/assets/plan_v1_m.png)
Der Gesamtplan mit Fahrradstraßen (grüne Punkte), umzubauenden Querungen Rad-/Autostraße (schwarze Punkte), vorhandenen und neu zu bauenden Tramlinien (rot/orange), Schnellbuslinien (lila) und vorhandene Bahnlinien mit neuen Haltepunkten (dunkelblau),  sowie Fußgänger\*innenbereiche und autofreie Innenstadt (blau unterlegt).

[Gesamtplan zum Download](/assets/plan_v1_m.png)

### Legende: 
**Fahrrad**
- Punkte dunkelgrün: Fahrradstraßen mit "Anlieger frei"
- Punkte dunkelgrün klein: breiter Fahrradweg entlang Straße
- Punkte hellgrün: Fahrradstraßen KFZ frei
- Punkte schwarz: Gefährliche Überquerung/Einfädelung zu Autostraße - hier sind Querungshilfen nötig (Markierungen, bauliche Anlagen, Tempobegrenzung usw.)

**ÖPNV**
- Rote Linien: vorhandene Linien der Tram
- Orange gepunktete Linien: notwendiger Ausbau des Tramnetzes
- Rote Punkte: Haltestellen an Tramlinien
- Lila Linien: Ring- und Schnellbuslinien
- Dunkelblaue Linien = S-Bahn / Regionalbahn

**Fußgänger\*innen**
- Hellblaue Flächen: autofreie Innenstadt und autofreie Zonen & Flaniermeilen

**Güterverkehr**
- Braune Linien: Schienen für den Güterverkehr und der Augsburger Localbahn

## Umsetzungs- Zeitplan
### Wir fordern ... sofort:

Ein Grundnetz miteinander verbundener Fahrradstraßen, auf denen höchstens noch Busse und Anwohner\*innen mit 20 km/h fahren bzw. auf ausgewiesenen Flächen parken dürfen. Radler\*innen haben stets Vorrang und dürfen nicht überholt werden.
Das Sofort-Fahrrad-Netz soll beinhalten:
- einen Weg um die Altstadt herum - soweit möglich auf viersprigen Straßen vom Autoverkehr abgetrennt in jeder Richtung auf zwei eigenen Spuren,
- Rad-Verbindungen quer durch die Innenstadt, mit weiterführenden  Achsen von dort in alle Ortsteile unter anderem entlang folgender Verläufe:
- Bismarckstraße - Von-der-Tannstr. - Hochfeldstr. - Von Richthofen-Straße
- Haunstetter Straße - Alter Postweg - Hugo-Eckener-Str. - Bürgermeister-Ulrich-Straße - Unterer Talweg - Postillonstr. bis nach Königsbrunn
- Hallstr. - Beethofenstr. - Hermanstr. - Rosenaustr. - Schießstättenstr. - Gollwitzerstr. - Färberstr. - Treustr. - Grasiger Weg
- Bahnhofstr. - Viktoriastr. - Fröhlichstr. - Pferseer Str. - Luipoldbrücke - Hessenbachstr. - Kazböckstr. - Eberleinstr. - Leonhard-Hausmann-Straße
- entlang der Friedbergerstr. nach Hochzoll und weiter nach Friedberg
- Vorfahrt oder Überquerungshilfen bei Kreuzungen mit Autostraßen
- Leihräder verteilt in der Stadt ohne Gebühr für kurze Strecken, darunter einige Lastenräder
- schnelles Umsetzen der Empfehlungen der Klimakom Studie mit Sofortmaßnahmen durch geänderte Beschilderung (geringer Kostenaufwand) zu Gunsten des Fahrradverkehr und für Fußgänger (z. B. Einbahnstraßen für Radverkehr freigeben)

Verbesserungen für Fußgänger\*innen und Belebung der Innenstadt
- systematische Beseitigung von Barrieren auf Fußverbindungen.
- Verbesserungen bei Fußgänger\*innenampeln: Maximal eine Ampelphase pro Überquerung, Erhöhung der Sicherheit (je nach Lage zusätzliche Gelbphase, Rundum-Grün für Fußweg über Autospuren usw.)
- Tempo 30 im gesamten Stadtgebiet, auch auf den Hauptstraßen. Bereiche mit geringerer Höchstgeschwindigkeit bleiben bestehen.
- Rundum-Ampeln an stark frequentierten Kreuzungen.
- Umsetzung der autofreien Hallstraße und Maximilianstraße sowie im Rahmen eines Verkehrsversuchs die autofreie Bahnhofstraße. Außerdem Sperrung der Altstadt für den Durchgangsverkehr.

Schnelle Aufwertung des ÖPNV durch:
- Nulltarif am Wochenende und an Feiertagen,
- 365€-Ticket für alle für den gesamten AVV-Verbundraum
- Freifahrschein für den gesamten AVV-Verbundraum für alle Senior\*innen, die ihren Führerschein abgeben.
- Verzicht auf Strafanzeigen für Schwarzfahren
- gekennzeichnete Mitfahr-Wartebereiche/-bänke am Stadtrand.
- Etablierung von Schnellbussen auf den Bundesstraßen mit enger Taktung im AVV-Gebiet
- Schaffung einer schnellen ÖPNV-Umfahrung von Augsburg
- Verknüpfung von DB-Bahnhöfen, Straßenbahnästen und Randzielen mittels Schnellbuslinien
- Einrichtung von Flexbussen, um Bahn und Schnellbuslinien ohne PKW erreichbar zu machen
- Einführung der S-Bahn im Taktverkehr: zwischen Meitingen und Bobingen (mit Elektrifizierung) und zwischen Neusäß (Reaktivierung drittes Gleis) zum Augsburger Hauptbahnhof nach Mering/Friedberg.

AVV muss drei Varianten für einen neuen Nahverkehrsplan entwickeln:
- 1) ÖPNV wie heute weiterbetreiben und aufrechterhalten trotz sinkender Einnahmen (Dies ist das, was der AVV heute als Aufgabe hat)
- 2) ÖPNV Nutzerzahlen verdoppeln bis 2026. Ausarbeitung eines "Nahverkehrsplanes x2" inklusive Ausarbeitung neuer Linien, Fahrzeugbedarf, Personalbedarf, Finanzierung,
- 3) ÖPNV Nutzerzahlen verdreifachen bis 2030.  Ausarbeitung eines "Nahverkehrsplanes x3" inklusive Ausarbeitung neuer Linien, Fahrzeugbedarf, Personalbedarf, Finanzierung.
- Mit diesem Nahverkehrsplan x2 und x3 und dem Finanzbedarf soll dann die Stadt/AVV beim Landtag / Bundestag um Fördermittel kämpfen. 
Beispiel [hier](https://www.verkehr4x0.de/wp-content/uploads/2021/10/211020_Buergerantrag_Nahverkehrsplan_V2.pdf) erklärt. 

Zudem Beschlüsse und Vorbereitung bzw. Planung für alle weiteren Maßnahmen, u.a. autofreie Zonen, Nulltarif und Ausbau der Tramlininen, Elektrifizierung der noch fehlenden Bahnstrecken und Reaktivierung und Neubau von Bahn Haltepunkten.
Alle Planungen für Neu- und Ausbau von Straßen werden gestoppt.


### 1-2 Jahre später sollte gelingen:
- Weitere Verdichtung des Fahrradstraßennetzes in alle Wohngebiete, zu Schulen, Kindergärten, Veranstaltungs- und Einkaufszentren, einschließlich des Umbaus aller Querungen von Autostraßen.
- Schaffung einer Fußgänger\*innenverbindung  als fahrzeugfreie Flaniermeile entlang des Äußeren Stadtgrabens. Umweltgerecht mit durchlässigem Pflaster, viel Grün und Renaturierung mit breiten Uferstreifen.
- Verlängerung der Fußgänger\*innenzone über die Bahnhofstraße zum Hauptbahnhof, entlang der Karolinenstraße ins Domviertel sowie die gesamte Altstadt.
- autofreie Zonen rund um Kliniken, Altenheime, Kindergärten und Grundschulen.

- Nulltarif in Bussen und Bahnen für Fahrgäste mit geringen Einkünften und als Prämie für den Verzicht auf ein privates Auto.
- Stärkung der Bahn mit weiteren Haltepunkten und Reaktivierung des dritten Gleises zwischen Neusäß und dem neuen Bahnhof Hirblinger Straße, um dichtere Taktzeiten zu erreichen (mehr Zugbegegnungen/-überholungen).
- Vollständige Elektrifizierung der Netze ins Allgäu
- ICE-Trasse nach München mit Weichen zu den Regionalverkehrsgleisen verbinden, um auf Störungen flexibel reagieren zu können
- Bau der dezentralen Mobilitätszentren (Schnellbuskreuz) mit Serviceangeboten (z. B. Gersthofen, Uni, Lechhausen AZ)
- Abschluss der Planungen und Machbarkeitsstudien für die neuen Tramlinien und den Ausbau des Schienennetzes der Deutschen Bahn 
- Weitere Haltestelle der S-Bahn/RB an der Fußballarena - wurde von der CSU schon 2008 gefordert ([Link zum AZ-Artikel](https://www.augsburger-allgemeine.de/augsburg/Plaene-fuer-neues-FCA-Stadion-Mit-der-Bahn-zur-impuls-arena-id3953381.html)
- Eröffnung der Bahn Haltepunkte Neusäß-Vogelsang (Biburg) und Friedberg-Paar (Harthausen)
- Reaktivierung der nun elektrifizierten Staudenbahn (Bahnstrecke Gessertshausen - Langenneufnach)
- Eröffnung des Güterverkehrszentrums (GVZ)


### In den Jahren danach wird alles fertig:
- Autofreie Innenstadt (nur noch Tram, Busse, Anlieger\*innen, eingeschränkter Gütertransport mit maximaler Verlagerung auf Lastenräder).
- Vollendung des dichten Fahrradstraßennetzes im Stadtgebiet einschließlich Umbau der Querungen von Straßen (wie im Plan).
- Eröffnung aller neuer Tramlinien und ausgebauter Bahnlinien und Haltepunkte inklusive Taktverkehr
- Nulltarif für alle. Abschaffung aller Fahrkarteninfrastruktur und -bürokratie.
- Ausbau der Bahntrasse Oberhausen - Donauwörth (bzw. Meitingen) vierspurig (oder dreispurig) um einen engen Takt der S-Bahn zu ermöglichen ([Informationen zur S-Bahn Augsburg](https://de-academic.com/dic.nsf/dewiki/1214677#sel=))
- Trennung des Regional- und  Fernverkehrs zwischen Augsburg und Ulm auf eigenen zweispurigen Trassen
- Umsetzung des Regio-Schienen-Taktes (Zielnetz): enger Takt nach Gesserthausen, Friedberg, Mering, Meitingen und Bobingen, außerdem Taktverkehr nach Dinkelscherben, Fischach/Langenneufnach/Markt Wald, Donauwörth, Aichach und Buchloe
- Eröffnung der Güterzugumfahrungsstrecke rund um Augsburg


### Die weiteren Schritte zur autofreien Stadt 
Personenbeförderung in rohstoff- und platzfressenden Individualfahrzeugen Fahrzeugen ist ebenso menschen- und umweltfeindlich wie die ständigen Warenflüsse über riesige Entfernungen mit LKWs. Eine Verkehrswende muss daher letztendlich das komplett beenden. Der oben beschriebene Verkehrswendeplan ist daher nur ein erster Schritt. 

### Die weitere Vision: Kein Autoverkehr mehr durch die Stadt
Autofahrten und Gütertransporte sind nur noch von außen in die Stadt möglich, nicht mehr quer durch diese. Vom außen gibt es ausgewählte Straßen, über die Richtung Stadtmitte gefahren werden kann. Abbiegen in Wohngebiete oder zu anderen Zielen bleibt möglich. Allerdings kann von dort nur wieder über die gleiche Verbindung aus der Stadt hinausgefahren werden. Es gibt keinen Querverkehr mehr. Die entsprechenden Verbindungen sind unterbrochen, in Fahrradstraßen gewandelt oder verschwunden. Der ÖPNV hingegen verbindet die Stadtteile weiter - ebenso wie Fuß- und Radverbindungen. Vorbild für diese Verkehrsführung ist das niederländische Houten, wo eine solche Verkehrsplanung schon seit längerem verwirklicht ist und funktioniert. Dort gibt es deutlich höhere Quoten vor allem des Radverkehrs als in Augsburg. 

## Politik der Kurzen Wege
Am besten ist Verkehr, der gar nicht entsteht. Daher fordern wir die Wiederbelebung der Stadtteile von Augsburg und der Umlandgemeinden durch Einkaufsmöglichkeiten, kulturelles Leben und medizinische Versorgung. Supermärkte auf der „grünen Wiese“, weit entfernte Arbeitsplätze oder Behörden, neue Gewerbegebiete und Straßen müssen grundsätzlich vermieden werden. Wo neue Wohnbebauung entsteht, muss direkt mitgeplant werden, dass die Menschen dort alles vorfinden, was sie täglich brauchen - Lebensmittel, medizinische Versorgung, Kitas, Kulturzentren, öffentliche Grünflächen ...


## Interaktive Karte des Verkehrswendeplans
<iframe width="100%" height="600px" frameborder="0" allowfullscreen src="//umap.openstreetmap.de/de/map/verkehrswendeplan-augsburg_21845?scaleControl=true&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false&datalayers=99556%2C99351%2C99347%2C99453%2C99560%2C99346%2C99350%2C99348%2C100548%2C99938%2C100881%2C101168%2C103728%2C99454#15/48.3663/10.8953"></iframe><p><a href="//umap.openstreetmap.de/de/map/verkehrswendeplan-augsburg_21845">Vollbildanzeige</a></p>


