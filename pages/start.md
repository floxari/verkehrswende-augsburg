---
layout: home
title: Start
permalink: /
nav_order: 10
---


### _Unsere Verkehrs-Utopie für Augsburg... eine autofreie Innenstadt und ein Plan für Fahrradstraßen, Verlängerung der Tramlinien, Schnellbusse zur Verknüpfung zwischen den Tramlinien, Reaktivierung von Schienenkilometern und mehr Haltestellen und Fußgänger\*innen-Meilen - dazu der Nulltarif. Platz fürs Leben statt Lärm, Stau und Stress!_

### Unsere Ideen für eine Stadt für Menschen und klimafreundliche Mobilität für alle

Auf dieser Homepage findet ihr viele Informationen und eine  umfangreiche Sammlung an Ideen wie der Verkehr und die Verkehrsinfrastruktur geändert werden könnte. 


![Grafik: Critical Mass 2018](/assets/cm-aux-2018.jpg)

Der öffentliche Raum in Augsburg ist wertvoll und begrenzt. Seit Jahren steigt die Zahl der Autos stetig. Das Ergebnis: Lärm, Stau und Stress. Autos sind keine schlauen Vehikel. Wir nehmen die Folgen nur nicht mehr wahr, weil alles so alltäglich scheint: Lärm, Abgase und  Feinstaub  durch  Reifen- und Bremsenabrieb, dazu ständige Gefahr mit täglich acht bis neun Verkehrstoten und 1053 Verletzten allein in Deutschland (3700 Tote weltweit, Stand: 2019). Wir transportieren ein bis zwei Tonnen Stahl für ein paar Kilo Mensch. Das kostet riesige Mengen Energie und CO2-Ausstoß.
Wenn sie nicht stehen, bewegen sie ein bis zwei Tonnen Stahl lärmend durch die Stadt, stinken dabei und verbreiten Feinstaub. Damit muss Schluss sein. Zu Fuß, mit dem Fahrrad, barrierefrei auch mit Gehhilfen und Rollstühlen und mit einem gut ausgebauten ÖPNV kommen wir ruhiger, sicherer und meist auch schneller an unser Ziel und die Stadt gewinnt dabei an Lebens- und Aufenthaltsqualität.

Zu Fuß, mit dem Rad und einem guten ÖPNV kämen wir entspannter, oft schneller und klimafreundlich ans Ziel.
Für die Mobilitätswende ist ein gut ausgebauter, schneller und gut vernetzter Nahverkehr und ein gut ausgebautes Radwegenetz unabdingbar. Ein Umstieg vom eigenen Auto auf den öffentlichen Nahverkehr ist für viele keine echte Alternative, da zu oft Orte nicht schnell und direkt verbunden sind.

Wir präsentieren hiermit unseren Verkehrswendeplan als umfassende Ideensammlung.
Alle Maßnahmen zusammen bieten die beste Lösung - damit sich am Ende alle gerne klimafreundlich fortbewegen und bequem und zuverlässig ans Ziel kommen.

(Viele unserer Ideen und Formulierungen sind angelehnt oder übernommen von der [Verkehrswende Initiative in Gießen](https://giessen-autofrei.siehe.website) oder im Austausch mit anderen Initiativen aus ganz Deutschland entstanden, etwa mit Koblenz oder Wolfsburg).


### Anmeldung zum Mailverteiler
Wenn Sie keine Infos zu Aktionen oder Veranstaltungen verpassen wollen, einfach eine leere Mail an verkehrswende-augsburg-subscribe@lists.riseup.net senden, dann werdet ihr aufgenommen.

Wenn ihr euch wieder abmelden möchtet, einfach eine leere Mail an verkehrswende-augsburg-unsubscribe@lists.riseup.net senden, dann werdet ihr vom Mailverteiler entfernt.

## Wir fordern für eine lebenswerte Stadt: Verkehrswende jetzt!
Auf diesen Seiten findet Ihr einen Plan für den Weg zu einer Verkehrswende in und um Augsburg, die es in sich hat. Am Ende soll mindestens eine autofreie Innenstadt stehen, vielleicht auch eine ganz autofreie Stadt. Dazu darf aber nicht mehr so weitergewurschtelt werden, sondern es muss genau das gemacht werden, was in anderen Städten bereits klappte und erhebliche Teile des Autoverkehrs verlagerte.
- Autofreie Innenstadt
- Fahrradstraßen quer durch die Innenstadt und um die Altstadt mit zuführenden Achsen von dort in alle Ortsteile
- Zusätzliche Fußgänger\*innenmeilen
- Ausbau der Tramlininen, Ausbau der Bahnstrecken und Reaktivierung und Neubau von Bahnhaltepunkten
- Nulltarif (fahrscheinloses Fahren für alle)
- Etablierung von Schnellbussen auf den Bundesstraßen mit enger Taktung im AVV-Gebiet
- Verknüpfung von DB-Bahnhöfen, Straßenbahnästen und Randzielen mittels Schnellbuslinien
- Eine Vielzahl kleiner Verbesserungen bei Ampelschaltungen, Überquerungen, Bereitstellung von (Lasten-)Leihrädern, Werkstätten …

[Der Umsetzungszeitplan unserer Verkehrs-Utopie für Augsburg](/plan/)

[![Plan Vorschaubild](/assets/plan_v1_k.png)](/assets/plan_v1_m.png)
Der Gesamtplan mit Fahrradstraßen (grüne Punkte), umzubauenden Querungen Rad-/Autostraße (schwarze Punkte), vorhandenen und neu zu bauenden Tramlinien (rot/orange), Schnellbuslinien (lila) und vorhandene Bahnlinien mit neuen Haltepunkten (dunkelblau),  sowie Fußgänger\*innenbereiche und autofreie Innenstadt (blau unterlegt).
[Gesamtplan zum Download](/assets/plan_v1_m.png)

Der Flyer zum Verkehrswendeplan: [Download als PDF](assets/Flyer Verkehrswendeplan Augsburg.pdf)


## Zahlen und Fakten

### Verkehrsunfälle, Verletzte und Tote im Augsburger Stadtgebiet
![Grafik: Unfallzahlen](/assets/UnfallzahlenAugsburg.png)

Quelle: [AZ vom 20.02.2023](https://www.augsburger-allgemeine.de/augsburg/augsburg-mehr-unfaelle-im-jahr-2022-vier-menschen-starben-in-augsburg-id65598591.html)
Über 8000 Unfälle ereignen sich jedes Jahr auf Augsburgs Straßen, dabei werden mehr als 1300 Menschen verletzt und pro Jahr sterben durchschnittlich 4-5 Personen. Tote Asphalt- und Betonwüsten prägen das Stadtbild. 

Darauf stehen 0,5 Autos je Einwohner*in in Augsburg (Kinder und andere Menschen ohne Führerschein mitgezählt) im Schnitt 23 Stunden am Tag. Wenn sie nicht stehen, bewegen sie ein bis zwei Tonnen Stahl lärmend durch die Stadt, stinken dabei und verbreiten Feinstaub. Damit muss Schluss sein. Zu Fuß, mit dem Fahrrad, barrierefrei auch mit Gehhilfen und Rollstühlen und mit einem gut ausgebauten ÖPNV kommen wir ruhiger, sicherer und meist auch schneller an unser Ziel und die Stadt gewinnt dabei an Lebens- und Aufenthaltsqualität. Augsburg kann autofrei, und das nicht nur in der Maxstraße.


Jede\*r Augsburger\*in legt am Tag 3,5 Wege mit einer durchschnittlichen Wegelänge von 7,8 km zurück. Für ungefähr 34% dieser Wege wird der PKW genutzt.  [Quelle und Weitere Fakten zum Verkehrsverhalten in Augsburg](https://www.augsburg.de/buergerservice-rathaus/verkehr/verkehrsverhalten)

[Bei 300.000 in Augsburg lebenden Menschen und rund 75.000 Pendlerinnen und Pendlern täglich können die Ziele und Bedürfnisse kaum unterschiedlicher sein.](https://www.augsburg.de/buergerservice-rathaus/verkehr/augsburger-mobilitaetsplan)

### Entwicklung der KFZ-Dichte in Augsburg von 2008 bis 2018
![Grafik: Kfz-Dichte](/assets/kfz_dichte.png)

Quelle: Statistisches Jahrbuch der Stadt Augsburg 2018

Im Ballungsraum Augsburg wohnen über 800.000 Menschen, die sich auf unterschiedliche Art fortbewegen. Sei es zu Fuß, mit dem Fahrrad, dem eigenen Auto, Dienstwagen … oder dem öffentlichen Nahverkehr. Die stark ansteigende Zahl von Fahrzeugen führt zu vollen Straßen und Stau. Die letzten Jahrzehnte wurde vergeblich versucht, mit Straßenneubauten und -ausbauten dem Verkehrskollaps entgegenzutreten. Auch technologische Weiterentwicklungen in den KFZ erweisen sich nicht als Lösung.
Der Nahverkehr stagniert seit über 10 Jahren auf einem ähnlichen Niveau. Für die Verkehrswende ist aber ein gut ausgebauter, schneller und gut vernetzter Nahverkehr unabdingbar.
Ein Umstieg vom eigenen Fahrzeug auf den öffentlichen Nahverkehr ist aktuell jedoch oft noch keine Alternative, da schnelle Verbindungen fehlen oder die Fahrzeit deutlich länger ist im Vergleich zum PKW. 




