---
layout: page
title: Fahrradstraßen
permalink: /fahrradstraßen/
nav_order: 40
---


## Fahrradstraßen, Radschnellverbindungen in alle Richtungen
Als Hauptsäule unseres Verkehrswendekonzepts wollen wir **mindestens 50% des Alltagsverkehrs aufs Fahrrad verlagern**, also für Strecken von und zur Arbeit, Ausbildung, Einkaufen. Dass dies möglich ist, zeigen Städte, die dem Radverkehr längere Zeit Vorrang einräumen und so 45 bis 60% Fahrradanteil erreichen (z. B. Houten und Groningen in den Niederlanden, Oldenburg, Münster und Greifswald hierzulande, Kopenhagen und andere Orte in Dänemark). Fahrradstraßen sind dabei das Rückgrat eines dichten und gut zu befahrenden  Radwegenetzes. Um nicht noch mehr Flächen zu versiegeln und Platz zu vergeuden, ist die **Umwidmung vorhandener Autostraßen** dem Neubau von Radwegen unbedingt vorzuziehen. Die Autos sollen ja schließlich raus. Platz ist also genug da und wird mit der Abkehr vom Auto sogar noch weiter frei.
Wir schlagen ein dichtes Fahrradstraßennetz zwischen den Stadtteilen und zur Innenstadt hin vor.

<iframe width="100%" height="600px" frameborder="0" allowfullscreen src="//umap.openstreetmap.de/de/map/verkehrswendeplan-augsburg_21845?scaleControl=true&miniMap=false&scrollWheelZoom=true&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false&datalayers=99351%2C99348%2C100548%2C99938%2C101168%2C103728"></iframe><p><a href="//umap.openstreetmap.de/de/map/verkehrswendeplan-augsburg_21845">Vollbildanzeige</a></p>

**Fahrrad**
- Punkte dunkelgrün: Fahrradstraßen mit "Anlieger frei"
- Punkte dunkelgrün klein: Breiter Fahrradweg entlang Straße
- Punkte hellgrün: Fahrradstraßen KFZ frei
- Punkte schwarz: Gefährliche Überquerung/Einfädelung zu Autostraße - hier sind Querungshilfen nötig (Markierungen, bauliche Anlagen, Tempobegrenzung usw.)


**Fahrradstraßen** sind besonders gekennzeichnete Bereiche (meist mit Schild plus Bodenmarkierung), in denen besondere Regeln gelten. Autos dürfen grundsätzlich maximal 30 km/h fahren und Radler\*innen, die auch nebeneinander fahren dürfen, nicht drängeln oder überholen. Wir lehnen es aber ab, die Fahrradstraßen grundsätzlich für Autos, d. h. auch für den Durchgangsverkehr freizugeben. Stattdessen schlagen vor: 
- Anlieger\*innen dürfen mit Autos zu ihren Grundstücken fahren, allerdings nur bis 20km/h schnell. Fahrräder haben prinzipiell Vorrang und dürfen nicht überholt werden.
- An geeigneten Stellen sollen die Straßen durch Autosperren unterbrochen werden, um sicherzustellen, dass kein Durchgangsverkehr über die Fahrradstraßen läuft. Verkehrsberuhigende Pflasterungen müssen so beschaffen sein, dass Radler\*innen dadurch nicht gestört, gefährdet oder ausgebremst werden.
- An Kreuzungen mit anderen Straßen gelten die üblichen Verkehrsregeln (Ampeln, rechts vor links usw.). Wo immer möglich, sollte die Fahrradstraße die Vorfahrtstraße bilden und das auch auf dem Boden entsprechend erkennbar sein.
- Querungen stark befahrener Autostraßen sollten besonders gestaltet werden, unter anderem mit einer baulichen Erhöhung, die Autos zum Abbremsen zwingt.
- Auf vierspurigen Straßen sollen zwei Spuren zu (baulich abgetrennten) Fahrradstraßen umgewandelt werden, dies kann auch anstatt von Parkplätzen am Straßenrand passieren.



### Wir fordern ... sofort:

Ein Grundnetz miteinander verbundener Fahrradstraßen, auf denen höchstens noch Busse und Anwohner\*innen mit 20 km/h fahren bzw. auf ausgewiesenen Flächen parken dürfen. Radler\*innen haben stets Vorrang und dürfen nicht überholt werden.
Das Sofort-Fahrrad-Netz soll beinhalten:
- einen Weg um die Altstadt herum - soweit möglich auf viersprigen Straßen vom Autoverkehr abgetrennt in jeder Richtung auf zwei eigenen Spuren,
- Rad-Verbindungen quer durch die Innenstadt, mit weiterführenden  Achsen von dort in alle Ortsteile unter anderem entlang folgender Verläufe:
- Bismarckstraße - Von-der-Tannstr. - Hochfeldstr. - Von Richthofen-Straße
- Haunstetter Straße - Alter Postweg - Hugo-Eckener-Str. - Bürgermeister-Ulrich-Straße - Unterer Talweg - Postillonstr. bis nach Königsbrunn
- Hallstr. - Beethofenstr. - Hermanstr. - Rosenaustr. - Schießstättenstr. - Gollwitzerstr. - Färberstr. - Treustr. - Grasiger Weg
- Bahnhofstr. - Viktoriastr. - Fröhlichstr. - Pferseer Str. - Luipoldbrücke - Hessenbachstr. - Kazböckstr. - Eberleinstr. - Leonhard-Hausmann-Straße
- entlang der Friedbergerstr. nach Hochzoll und weiter nach Friedberg
- Vorfahrt oder Überquerungshilfen bei Kreuzungen mit Autostraßen
- Leihräder verteilt in der Stadt ohne Gebühr für kurze Strecken, darunter einige Lastenräder
- schnelles Umsetzen der Empfehlungen der Klimakom Studie mit Sofortmaßnahmen durch geänderte Beschilderung (geringer Kostenaufwand) zu Gunsten des Fahrradverkehr und für Fußgänger (z. B. Einbahnstraßen für Radverkehr freigeben)
- Tempo 30 im gesamten Stadtgebiet, auch auf den Hauptstraßen. Bereiche mit geringerer Höchstgeschwindigkeit bleiben bestehen.
- Rundum-Ampeln an stark frequentierten Kreuzungen.
- Umsetzung der autofreien Hallstraße und Maximilianstraße sowie im Rahmen eines Verkehrsversuchs die autofreie Bahnhofstraße. Außerdem Sperrung der Altstadt für den Durchgangsverkehr.

Zudem Beschlüsse und Vorbereitung bzw. Planung für alle weiteren Maßnahmen, u.a. autofreie Zonen, Nulltarif und Ausbau der Tramlininen, Elektrifizierung der noch fehlenden Bahnstrecken und Reaktivierung und Neubau von Bahn Haltepunkten.
Alle Planungen für Neu- und Ausbau von Straßen werden gestoppt.

### 1-2 Jahre später sollte gelingen:
- Weitere Verdichtung des Fahrradstraßennetzes in alle Wohngebiete, zu Schulen, Kindergärten, Veranstaltungs- und Einkaufszentren, einschließlich des Umbaus aller Querungen von Autostraßen.

### In den Jahren danach wird alles fertig:
- Autofreie Innenstadt (nur noch Tram, Busse, Anlieger\*innen, eingeschränkter Gütertransport mit maximaler Verlagerung auf Lastenräder).
- Vollendung des dichten Fahrradstraßennetzes im Stadtgebiet einschließlich Umbau der Querungen von Straßen (wie im Plan).


## Mehr Infos:
- Was ist eine Fahrrradstraße? Beitrag des Hamburger Senats (Der Senat ist die Landesregierung der Freien und Hansestadt Hamburg) am 6.5.2016
<iframe width="560" height="315" src="https://www.youtube.com/embed/YS_WC21Zims" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Extra Seite zu Fahrradstraßen: [fahrrad.siehe.website](fahrrad.siehe.website)

- Die Stadt Augsburg lobt sich selbst für ihre Radwege: [https://www.augsburg.de/buergerservice-rathaus/verkehr/radverkehr/neue-radwege](https://www.augsburg.de/buergerservice-rathaus/verkehr/radverkehr/neue-radwege)

## Detailvorschläge zur Fahrradinfrastruktur
**Umwandlung von zwei der vier vorhandenen Autospuren zu Fahrradstraßen**

Zwischen Theodor-Heuss-Platz (Richtung Haunstetter Str.) und Rotem Tor und FH: Fahrradstraßen in beide Richtungen mit mindestens zwei Meter Breite, dafür Fahrbahnspuren des MIV umwandeln mit baulicher Abtrennung. Weiterführung der Fahrradstraße über Konrad-Adenauer-Allee und Fuggerstraße zum Staatstheater sowie über Friedberger und Haunstetter Straße in die Stadtteile (auch jeweils mindestens 2m breite Radwege baulich vom MIV abgetrennt)

Diese Umwandlung von zwei der vier vorhandenen Autospuren zu Fahrradstraßen zwischen Theodor-Heuss-Platz und Rotem Tor und weiter entlang der Friedberger Straße nach Friedberg ist zentral für eine attraktive, durchgängige und schnelle Radvorrangverbidnung zwischen Stadtteilen und Innenstadt. Zudem vom Roten Tor aus entlang der Haunstetter Straße nach Königsbrunn und auf der Gögginer Straße von der Gögginger Brücke bis zur Haltestelle Burgfrieden. Auch nötig ist dies zwischen Plärrer und Staatstheater (Klinkerberg, Schaetzlerstraße, Volkhartstraße und Gesundbrunnenstraße) und von der Fuggerei über Jakobertor zur Ulrichsbrücke.

In der Wertachstraße und weiterführend in der Donauwörtherstr. zum Bärenwirt ist auch die Umwandlung von Parkplätzen am Straßenrand notwendig. Selbiges gilt auch im Oberen Graben und in der Rosenaustraße. Die Parkplätze müssen wegfallen, um Platz für eine klimafreundliche und sichere Mobilität zu schaffen.

Es ist notwendig, dass Radverkehr nicht mehr länger auf engen Wegen neben dem MIV mit vielen kleinen Bordsteinen, Schlaglöchern und anderen Gefahrenstellen Platz findet, sondern den Platz bekommt, den der Fahrradverkehr zukunftsweisend für einen hohen Anteil am Verkehr benötigt und der ihm zusteht. Wer Fahrradstraßen sät, wird Radverkehr ernten.

**Kreuzungen übersichtlich gestalten**
An den Kreuzungen sollen Radabstellanlagen auf der Fahrbahn erstellt werden, sodass man als Verkehrsteilnehmer gut in die Kreuzung einsehen kann, was bei abgestellten Rädern sehr gut funktioniert, aber bei abgestellten KFZ meistens nicht mehr.
Klassisches Beispiel ist am Kreuzungskonglomerat von Obstmarkt, Kesselmarkt und Johannisgasse. Die Kreuzung ist aktuell vom Obstmarkt sehr schwer einsehbar, selbst wenn alle Autos legal parken. Wenn im Kesselmarkt und in der Johannisgasse die ersten fünf Meter Parkraum entfallen würden, wäre die Kreuzung wesentlich überschaubarer.

Dafür gibt es im Stadtgebiet noch wesentlich mehr geeignete Orte, wie die Kreuzung "Beim Hafnerberg" und "Heilig-Kreuz-Straße". Wenn rechts vom Hafnerberg in der Heilig-Kreuz-Straße zwei Parkplätze entfallen würden, hätte man als Radfahrer den Überblick über die Kreuzung, weil man die Autos von rechts zur Zeit praktisch nicht sehen kann.


## Mängelmelder Stadt Augsburg
Mängel auf Radwegen können jetzt einfach und schnell an die Stadt Augsburg gemeldet werden: Radlerinnen und Radler können Verkehrs-Hindernisse und Störungen im Stadtgebiet an die Verwaltung melden – online oder per App.
https://www.augsburg.de/buergerservice-rathaus/verkehr/radverkehr/maengelmelder-radverkehr

## Fahrradprojekte in und um Augsburg
**Jeden letzten Freitag im Monat um 18Uhr am Rathausplatz: Critical Mass**
[https://criticalmass-augsburg.de/](https://criticalmass-augsburg.de/)
Eine Gruppe von Radfahrer\*innen fährt gemeinsam durch die Stadt – wir sind der Verkehr!

![Grafik: Critical Mass Plakat](/assets/CM-Western.jpg)

**Radeln zur Uni - Wir fahren gemeinsam als Critical Mass jeden Tag zur Uni**
Treffpunkte, Uhrzeit und Orte finden sich auf der Homepage:
[https://www.radeln-zur-uni.de/](https://www.radeln-zur-uni.de/)

**Fahrradselbsthilfewerkstatt Bikekitchen**
Heilig-Kreuz-Straße 30 (Do. 18-20 Uhr) 
[http://www.bikekitchen-augsburg.de/](http://www.bikekitchen-augsburg.de/)

### Verleih und hilfreiche Orte beim Rad fahren in Augsburg
**Lastenradverleih Max & Moritz – die Statt Transporter zum Ausleihen**
[https://max-und-moritz.bike/](https://max-und-moritz.bike/)

**Augsburger Leih-Lastenrad LECH ELEPHANT**
Seit Mai 2023 neu mit 11 E-Lastenrädern in der ganzen Stadt verteilt.
[https://lech-elephant.de/](https://lech-elephant.de/)

**Fahrradverleih der SWA und Nextbike** [https://www.swa-rad.de/de/augsburg/](https://www.swa-rad.de/de/augsburg/)

Ein Leihlastenrad gibt es auch beim Freiwilligen-Zentrum Augsburg [https://www.freiwilligen-zentrum-augsburg.de/unsere-projekte/leihla/](https://www.freiwilligen-zentrum-augsburg.de/unsere-projekte/leihla/)

Alle Angehörige der Hochschule können bei der Hochschschule ein Lastenrad kostenlos ausleihen [https://www.hs-augsburg.de/Fahrradfreundliche-Hochschule.html](https://www.hs-augsburg.de/Fahrradfreundliche-Hochschule.html)

**Fahrradreparaturstation und Schlauchautomaten**
- Vor dem Fahrradladen Dynamo am Oberen Graben und an der Hochschule am Bahnhof Haunstetter Straße gibt es jeweils einen Schlauchautomaten, an dem rund um die Uhr Fahrradschläuche erhältlich sind
- zudem gibt es einen Schlauchautomat vor dem Fahrradladen im Univiertel (Salomon-Idler-Str 26) und vor dem Stadtberger Bad sowie vor dem Fahrradladen an der Friedbergerstr. 107 sowie in der Lise-Meitner-Str 6
- An der Hochschule neben dem Schlauchautomat gibt es eine Fahrrad Servicestation mit Fahrradpumpe 
- Am Eingang zum Sportzentrum der Universität Augsburg gibt es eine Servicestation mit Fahrradpumpe
- Im Jahr 2023 sollen auf dem Campus der Universität 3 weitere Reperaturstationen entstehen, u.a im Innenhof des D-Gebäudes, bei der Sowi-Bib und an der Tramhaltestelle/Mensa
- Fahrradpumpe am Königsplatz am Manzu-Brunnen
- Im Klimacamp am Fischmarkt kann auch immer nach Reperaturwerkzeug oder einer Fahrradpumpe gefragt werden, es ist i. d. R. alles nötige vorhanden

**Informationen über E-Bike und Pedelec-Ladestationen in Augsburg:**
[https://a2011.wordpress.com/2021/06/11/e-bike-und-pedelec-ladestationen-in-augsburg/](https://a2011.wordpress.com/2021/06/11/e-bike-und-pedelec-ladestationen-in-augsburg/)

**Beratung zur gewerblichen Nutzung von Lastenrädern gibt es unter anderem bei Elephant Cargo** [https://elephant-cargo.com/](https://elephant-cargo.com/)

Link zu den Augsburger Radzählstationen:
Karte: [https://data.eco-counter.com/ParcPublic/?id=5417](https://data.eco-counter.com/ParcPublic/?id=5417)
Daten: [https://www.augsburg.de/buergerservice-rathaus/verkehr/radverkehr/radzaehlstationen](https://www.augsburg.de/buergerservice-rathaus/verkehr/radverkehr/radzaehlstationen)
Link zum Unfallatlas mit Personenschäden: [https://unfallatlas.statistikportal.de/](https://unfallatlas.statistikportal.de/)

## Verbesserungen und Vorschläge für die Fahrrad-Infrastruktur in Augsburg: 

- Hochzoll - Kreuzung Peterhofstraße/Höfatstrasse: Fahrradstraße (normal) bis zum Lechufer  einrichten, im Kreuzungsbereich Friedberger Überholverbote für MIV, damit Schüler*innen sicher zum Rudolf-Diesel-Gymnasium kommen. Vor der Schule Parktaschen entfernen und Gehweg verbreitern. Video: [https://youtu.be/A-UNESJxVUQ](https://youtu.be/A-UNESJxVUQ)

- Hochzoll - Waxensteinstraße: Fahrradstraße (normal) mit Einbahnstraßenregelung für KFZ von Nord nach Süd (morgens KFZ Schleichweg)

* überdachte Abstellanlagen für eBikes und für Lastenräder schaffen, mit Ladesäulen, im Innestadtbereich zuerst

* physische Barrieren zwischen Fahrrad und MIV 

Überprüfung aller Anordnungen per VZ 240 (besonders vor Unterführungen); Fahrtrecht für Radfahrer auf regulärer Fahrbahn und Anordung von VZ 260. (z.B. Unterführung Alter Heuweg, [https://youtu.be/F1pnxTak0bs](https://youtu.be/F1pnxTak0bs))

Überprüfung aller einseitigen Radwege im Stadtgebiet, die in beide Richtungen benutzungspflichtig sind (z.B. Blücherstrasse zwischen Hochzoll u. Lechhausen): "Die Benutzung von in Fahrtrichtung links angelegten Radwegen in Gegenrichtung ist insbesondere innerhalb geschlossener Ortschaften mit besonderen Gefahren verbunden und soll deshalb grundsätzlich nicht angeordnet werden. [http://www.verwaltungsvorschriften-im-internet.de/bsvwvbund_26012001_S3236420014.htm](http://www.verwaltungsvorschriften-im-internet.de/bsvwvbund_26012001_S3236420014.htm))

* Schutzstreifen-Markierungen entfernen, wenn sich diese in der 80cm Dooringzone befinden (z.B. Stettenstrasse)

* Hochzoll - Friedberger Straße zwischen B2 und Lech: Der Radfahrstreifen verstößt gegen Mindestbreitenregelungen. Parkbuchten entfernen, Platz schaffen und neu verteilen. Video: [https://youtu.be/72r9yZMHgUQ](https://youtu.be/72r9yZMHgUQ)

* Radstreifen Georg-Haindl-Straße / Müllerstraße verbreitern, damit auch Schwerlastverkehr 1,50m Abstand beim Vorbeifahren halten kann ([https://youtu.be/-dihMHq9DB8](https://youtu.be/-dihMHq9DB8))

* Friedberg: an der Augsburger Straße beidseitig VZ 240 aufheben und geschützte Radspuren anlegen; Radfaherer biegen mit allen Vorfahrtsrechten auf eigner Spur auf der Fahrbahn ab an der B300

- Lechhausen/Hammerschmiede/Firnhaberau Radweg Schillstraße: es gibt beiderseits der Straße einen Radweg, aber der ist Stadtauswärts kaum mehr befahrbar, eher Schlagloch-Wurzel-Holperpiste (via Twitter, Nick: intoleranter Pyromane @twatschi)

- Vor dem Netto im Postweg sind Wurzeln die den Asphalt hochdrücken auf dem Fuß- und Radweg

- D9 Fernradweg: Querung der ST2380 zwischen Mering und Königsbrunn für Radfahrer und Fußgänger sicherer machen bzw. kurze Wege an das andere Lechufer (Staustufe 23) ermöglichen. Fahrbahnrand im Wegebereich abpollern, um Falschparken zu verhindern. Aktueller Zustand dokumentiert hier: [https://youtu.be/Vyxqlb-6140](https://youtu.be/Vyxqlb-6140)

- Rosenaustraße: zwischen Ravenspurger Straße und Gögginger Straße - Radfahrer Linksabbieger (Angstweiche) an der Gögginger Straße anders lösen
