---
layout: page
title: Presse
permalink: /presse/
nav_order: 105
---

## Pressemitteilungen:
(diese Seite wird erst seit 2023 gepflegt, ältere ressemitteilungen sind noch nicht eingearbeitet)

### Pressemitteilung vom 22.02.2023

Klimaaktivist*innen rufen zu Fahrraddemonstration über die A8 auf
Sie fordern ein generelles Tempolimit und den Ausbaustopp des Autobahnnetzes, ein Moratorium für den Bundesverkehrswegeplan und stattdessen Investitionen in das Schienennetz und den ÖPNV.

Im Rahmen eines bundesweiten Aktionswochenendes werden in Augsburg Aktivist*innen am Sonntag, dem 5. März eine Fahrrad-Großdemonstration veranstalten. Beginn der Kundgebung ist um 14:00 Uhr am Rathausplatz, um 14:20 wollen sie zur Autobahn A8 losfahren, um dort für eine lebenswerte Welt zu demonstrieren. Auch werden gerade Zubringer-Demonstrationen und Treffpunkte organisiert, die von Neusäß und Haunstetten aus zu der Großdemonstration fahren sollen.

Die Aktionen im Rahmen des deutschlandweiten Aktionstages richten sich gegen die Pläne der Bundesregierung, den Ausbau von über 140 Autobahnprojekten zu beschleunigen.
Wenn es nach den Plänen der FDP geht, soll künftig der Ausbau des Straßennetzes deutlich schneller vorangetrieben werden. Konkret sollen 144 Autobahnen in einer Gesamtlänge von über 1.300 Kilometern [1] ohne jegliche Umweltverträglichkeitsprüfungen auf bis zu 10 Spuren ausgebaut werden. Laut BUND Naturschutz [1] bedeutet dies eine zusätzliche CO2-Belastung von über 400.000 Tonnen pro Jahr und eine direkte Bedrohung von 80 Naturschutzgebieten.

"Die  Autobahnfantasien der Bundesregierung sind ein Skandal, denn sie sind weder mit den Klimazielen der Bundesregierung vereinbar noch mit unserem Grundgesetz, das den Staat dazu verpflichtet, die natürlichen Lebensgrundlagen auch für zukünftige Generationen zu bewahren", so Ingo Blechschmidt (34) vom Klimacamp.

"Mit der aktuellen Politik zeigt das Verkehrsministerium einmal mehr ein klares Desinteresse an einer Kehrtwende hin zu einer nachhaltigen, sozial- und umweltverträglichen Mobilität", so Blechschmidt. Er bezieht sich damit exemplarisch auf das deutschlandweite ÖPNV-Ticket Bezug, welches ursprünglich als Nachfolgelösung des 9€-Tickets geplant war, aber schlussendlich mit monatelanger Verzögerung und einem Preis von 49€/Monat kommen soll. "Diese Lösung ist nicht sozial gerecht und völlig unzureichend, um einen permanenten Umstieg auf den ÖPNV zu ermöglichen. Doch anstatt sich für das Gemeinwohl einzusetzen, ist das Verkehrsministerium nur daran interessiert, möglichst lange den Wahnsinn des motorisierten Individualverkehr weiterzuführen", so Blechschmidt weiter.

Daher fordern die Aktivist*/innen von der Bundesregierung ein Tempolimit für den Autobahnausbau: "Ein Gesetz, das einen beschleunigten Autobahnausbau ermöglicht, ist eine radikale Kampfansage an die zukünftigen Generationen", so Laurenz Werner (19). Werner und seine Mitstreiter*innen folgen damit der Darstellung von UN-Generalsekretär António Guterres, der Staaten, die die Abhängigkeit von fossilen Energieträgern ausbauen, als die eigentlich Radikalen sieht [4]. Sie fordern weiterhin ein Moratorium für den Bau aller Autobahnprojekte. Zudem fordern die Aktivisten ein generelles Tempolimit auf allen Autobahnen. Dies würde zu einer deutlichen und schnell wirkenden Reduktion des CO2-Ausstoßes führen [5] und die Unfall- und Staugefahr drastisch mindern.

Zudem sehen die Aktivist*innen mit Blick auf die Ukrainekrieg noch einen ganz anderen Vorteil: "Mit einem Tempolimit von 100 km/h könnten wir jedes Jahr 2,1 Milliarden Liter Rohölimporte einsparen", erklärt xxx mit Verweis auf eine Studie des Umweltbundesamt [6]. "Das sind 2,1 Milliarden Liter, die kein weiteres Geld für Putins Kriegskasse liefern."

"Die Fakten sind klar: Jeden Tag trauern in Deutschland acht Familien, weil es zu einem tödlichen Verkehrsunglück kam. Jeder Verkehrstod traumatisiert 113 Menschen, von Ersthelfenden bis zu Familie und Kolleg*innen, 880 Personen werden täglich bis zu schwerst verletzt [3] - wir haben keine andere Möglichkeit als uns gegen diese gefährliche und rücksichtslose Verkehrspolitik für Mensch und Umwelt gegenüber der Regierung zu wehren und dagegen zu demonstrieren", so Blechschmidt.


REFERENZEN
[1] https://www.bund.net/service/publikationen/detail/publication/faktenblatt-bund-auswertung-autobahn-projektliste-zur-engpassbeseitigung/
[2] https://www.bvwp-projekte.de/map_street.html
[3] aus Veröffentlichungen der Bestseller-Autorin Katja Diehl, u.a https://www.fischerverlage.de/buch/katja-diehl-autokorrektur-mobilitaet-fuer-eine-lebenswerte-welt-9783103971422
[4] https://www.rnd.de/politik/antonio-guterres-bezeichnet-neuen-klimabericht-als-dokument-der-schande-XLA2Z4M2HPT7WCMKNPVAYSNUMM.html
[5] https://www.zdf.de/nachrichten/panorama/tempolimit-co2-ausstoss-senkung-faq-100.html
[6] https://www.tagesschau.de/wirtschaft/technologie/einsparpotenzial-tempolimit-101.html

ABLAUF
14:00 Sammlung, Reden und Aufstellung des Zuges am Rathausplatz
14:20 Abfahrt am Rathausplatz
14:50 Auffahrt auf die Autobahn A8
15:30 Ankunft am Rathausplatz mit Abschlussreden

Start am Augsburger Rathausplatz. Von dort durch die Innenstadt zur MAN-Brücke. Weiter über die B2 bis zur A8-Auffahrt 73 (Augsburg-Ost). Dort kurze Zwischenkundgebung in Vorbereitung der polizeilichen A8-Sperrung. Dann für 2 km (7 Minuten) über die A8 bis zur Abfahrt 74a (Friedberg). Von dort zurück zum Rathausplatz.  Die Route ist ca. 16km lang.

